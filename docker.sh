#!/usr/bin/env bash

#function name
CMD=

#docker images:tag
IMAGE=

#workspace
BASE_PATH='.' 

#-e
PWD='123456'

#optional --name
NAME_CMD=

#-p
PORT=

#?
FILE=

#output command
OUTPUT=

#--net --ip
SUB_NET=
NET_IP=

#-e
ADDTION_PARAMS=

#init
SQL=

#
C_NAME=
WORD_DIR=
CONTAINER_ID=

#default folder name
FOLDER_NAME=

#--name
NAME=

DEFAULT_IMAGE=
DEFAULT_PORT=
DEFUALT_USERNAME=

E1=
V1=
P1=

#color controller
function green(){
  echo -e "\E[1;32m$1\E[0m"
}
function red(){
  echo -e "\E[1;31m$1\E[0m"
}
function yellow(){
  echo -e "\E[1;33m$1\E[0m"
}

function help(){
    green "ok"
    red "fail"
    yellow "warn"
    echo -e "$ $0 -s|--start {mysql|redis} [-n|--name {container_name}] [-b|--path {base_path}] [--pwd|--password {password}] [-p|--port {port}] [-i|--image {image:tag}]"
    echo -e "$0 -s mysql -d . -n mysql --pwd 12345678 -p 3306 -i mysql:5.7"
    echo -e "$0 -s redis -d . -n redis --pwd 12345678 -p 6379 -i redis:latest "
    echo -e "$0 -s nginx -d . -n nginx  -p 8080 -i nginx:latest "
    echo -e "$0 -s filebrowser -d . -n file  -p 8099 -i filebrowser/filebrowser:latest "
    echo -e "$0 -s fdfs --name fdfs  --params '-e IP=172.17.0.1 -e WEB_PORT=8098' "
    echo -e "$0 -s nginx-vsftp -d . -n imagesserver  -p 8099 -i nginx:latest "
    echo -e "$0 -s mongo -d . -n mongo --username root --pwd 12345678 -p 27017 -i mongo:latest "
    echo -e "$0 -s postgres -d . -n postgres  --pwd 12345678 -p 54321 -i postgres:latest "

    
}

#fisco webase oracle mongo 
function exec_cmd(){

  EXEC_COMMAND="docker run ${NAME_CMD} -itd ${SUB_NET} ${NET_IP} ${E1} ${V1} --restart=always ${P1} ${IMAGE} ${ENDPOINT}"
  if [[ -n "$OUTPUT" ]]; then 
    echo "EXEC_COMMAND"
    echo "${EXEC_COMMAND}"
    echo "-----------------"
  else
    CONTAINER_ID=`${EXEC_COMMAND}`
    echo "[**]command:${EXEC_COMMAND}"
    C_NAME=${NAME}
    if [[ -z "${C_NAME}" ]]; then
      C_NAME="${CONTAINER_ID:0:12}" 
    fi
    green "Congraduations! Finished!"
    echo -e "---------------------"
    yellow "docker exec -it ${C_NAME} bash"
    yellow "docker stop ${C_NAME} && docker rm ${C_NAME} && rm -rf ${WORD_DIR}"
    docker ps | grep ${C_NAME}
  fi
}

function setDefaultConfig(){
    if [[ -z "${IMAGE}" ]]; then
    IMAGE=${DEFAULT_IMAGE} 
  fi
  if [[ -z "${PORT}" ]]; then
    PORT=${DEFAULT_PORT} 
  fi
  if [[ -z "${USERNAME}" ]]; then
    USERNAME=${DEFUALT_USERNAME} 
  fi
}

function new_workspace(){
  WORD_DIR="$(cd ${BASE_PATH}; pwd)/${FOLDER_NAME}"
  echo "[**]Work_dir is ${WORD_DIR}"
}


#FastDFS
function fdfs(){

  FOLDER_NAME="Fdfs"
  DEFAULT_IMAGE='qbanxiaoli/fastdfs:latest'
  DEFAULT_PORT=8095

#work space
  new_workspace
  data_path="${WORD_DIR}"
  mkdir -p ${data_path}

#param
  setDefaultConfig
#conf
  
#docker command

SUB_NET="--net=host"
E1=${ADDTION_PARAMS}
V1="-v ${data_path}:/var/local"
P1="-p ${PORT}:80"

}


function filebrowser(){
  
  FOLDER_NAME="FileBrowser"
  DEFAULT_IMAGE='filebrowser/filebrowser:latest'
  DEFAULT_PORT=8099

#work space
  new_workspace
  data_path="${WORD_DIR}/data"
  mkdir -p ${data_path}

#param #not need modifier
  setDefaultConfig
#conf
  
#docker command
#modifier
V1="-v ${data_path}:/srv"
P1="-p ${PORT}:80"

}

function mongo(){

  FOLDER_NAME="Mongo"
  DEFAULT_IMAGE='mongo:latest'
  DEFAULT_PORT=27017
  DEFUALT_USERNAME="admin"
  

#work space
  new_workspace

  data_path="${WORD_DIR}/data"
  mkdir -p ${data_path}
  conf_path="${WORD_DIR}/conf"
  mkdir -p ${conf_path}

#param
  setDefaultConfig

V1="-v ${data_path}:/data/db"
E1="-e MONGO_INITDB_ROOT_USERNAME=${USERNAME} -e MONGO_INITDB_ROOT_PASSWORD=${PWD}"
P1="-p ${PORT}:27017"

}

function postgres(){

  FOLDER_NAME="PostgreSQL"
  DEFAULT_IMAGE='postgres:latest'
  DEFAULT_PORT=27017
  DEFUALT_USERNAME="admin"
  
#work space
  new_workspace

  data_path="${WORD_DIR}/data"
  mkdir -p ${data_path}
  conf_path="${WORD_DIR}/conf"
  mkdir -p ${conf_path}

#param
  setDefaultConfig

V1="-v ${data_path}:/data/db"
E1="-e PGDATA=/var/lib/postgresql/data/pgdata -e POSTGRES_PASSWORD=${PWD} -e TZ='Asia/Shanghai'"
P1="-p ${PORT}:5432"

}


nginx_default_conf='server {
    listen       80;
    server_name  localhost;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    location /api{
        proxy_pass http://172.72.0.1:3306/api;
        # access_log "logs/test.log";
    }
}';

function nginx(){

  FOLDER_NAME="Nginx"
  DEFAULT_IMAGE='nginx:latest'
  DEFAULT_PORT=8080

  CONFIG_FILE=${nginx_default_conf}

#work space
  new_workspace

  data_path="${WORD_DIR}/HTML"
  mkdir -p ${data_path}
  conf_path="${WORD_DIR}/conf"
  mkdir -p ${conf_path}

#param
  setDefaultConfig
#conf
  CONF_FILE="${conf_path}/default.conf"

  #detect file exist
  if [[ -f "${CONF_FILE}" ]]; then
  yellow "${CONF_FILE} exist"
  else
  cat >${CONF_FILE} <<EOF
${CONFIG_FILE}
EOF
  fi
#docker command
V1="-v ${data_path}:/usr/share/nginx/html -v ${conf_path}:/etc/nginx/conf.d"
E1=
P1="-p ${PORT}:80"

}


function redis(){
  FOLDER_NAME="Redis"
  DEFAULT_IMAGE='redis:latest'
  DEFAULT_PORT=6379
  
#work space
  new_workspace

  data_path="${WORD_DIR}/data"
  mkdir -p ${data_path}
  conf_path="${WORD_DIR}/conf"
  mkdir -p ${conf_path}

#config
  CONF_FILE="${conf_path}/redis.conf"

  if [[ -f "${CONF_FILE}" ]]; then
  echo -e "${CONF_FILE} exist"
  else
  cat >${CONF_FILE} <<EOF
#bind 127.0.0.1
protected-mode yes
port 6379
tcp-backlog 511
timeout 0
tcp-keepalive 300
daemonize no
supervised no
pidfile /var/run/redis_6379.pid
loglevel notice
logfile ""
databases 16
always-show-logo yes
save 900 1
save 300 10
save 60 10000
stop-writes-on-bgsave-error yes
rdbcompression yes
rdbchecksum yes
dbfilename dump.rdb
dir ./
replica-serve-stale-data yes
replica-read-only yes
repl-diskless-sync no
repl-diskless-sync-delay 5
repl-disable-tcp-nodelay no
replica-priority 100
lazyfree-lazy-eviction no
lazyfree-lazy-expire no
lazyfree-lazy-server-del no
replica-lazy-flush no
appendonly no
appendfilename "appendonly.aof"
appendfsync everysec
no-appendfsync-on-rewrite no
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
aof-load-truncated yes
aof-use-rdb-preamble yes
lua-time-limit 5000
slowlog-log-slower-than 10000
slowlog-max-len 128
latency-monitor-threshold 0
notify-keyspace-events ""
hash-max-ziplist-entries 512
hash-max-ziplist-value 64
list-max-ziplist-size -2
list-compress-depth 0
set-max-intset-entries 512
zset-max-ziplist-entries 128
zset-max-ziplist-value 64
hll-sparse-max-bytes 3000
stream-node-max-bytes 4096
stream-node-max-entries 100
activerehashing yes
client-output-buffer-limit normal 0 0 0
client-output-buffer-limit replica 256mb 64mb 60
client-output-buffer-limit pubsub 32mb 8mb 60
hz 10
dynamic-hz yes
aof-rewrite-incremental-fsync yes
rdb-save-incremental-fsync yes
EOF
  fi

#exec docker

setDefaultConfig
V1="-v ${CONF_FILE}:/etc/redis/redis.conf -v ${data_path}:/data"
P1="-p ${PORT}:6379"
ENDPOINT="redis-server /etc/redis/redis.conf --appendonly yes --requirepass ${PWD}"

#output
  echo -e "docker exec -it ${NAME} redis-cli"
}


function mysql(){
  FOLDER_NAME="MySQL"
  DEFAULT_IMAGE='mysql:5.7'
  DEFAULT_PORT=3306

#work_space
  new_workspace

  data_path="${WORD_DIR}/data"
  mkdir -p ${data_path}
  conf_path="${WORD_DIR}/conf"
  mkdir -p ${conf_path}

#param
  setDefaultConfig

#conf
  CONF_FILE="${conf_path}/my.cnf"

  #detect file exist
  if [[ -f "${CONF_FILE}" ]]; then
  warn "${CONF_FILE} exist"
  else
  cat >${CONF_FILE} <<EOF
[client]
default-character-set=utf8mb4
[mysql]
default-character-set=utf8mb4
[mysqld]
server-id=1
EOF
  fi

#-e
E1="-e MYSQL_ROOT_PASSWORD=${PWD}"
V1="-v ${data_path}:/var/lib/mysql -v ${conf_path}:/etc/mysql/conf.d"
P1="-p ${PORT}:3306"

#exec docker

#output

  echo -e "=-=-=- INFO 8 -=-=-= "
  echo -e "docker exec -it ${NAME} mysql -uroot -p${PWD} -h 127.0.0.1"
  echo -e "use mysql;"
  echo -e "update user set host = '%' where user = 'root';"
  echo -e "flush privileges;"
  
  
}



nginx_imageserver_conf='server {
    listen       80;
    server_name  localhost;

    #(5)
    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    #(1)
    location /images/ {
        root  /mnt/;
        autoindex on; #(2)
        autoindex_exact_size off; #(3)
        autoindex_localtime on; #(4)
        charset utf-8,gbk; #(5)
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}';

function nginx-vsftp(){

  FOLDER_NAME="FTP"
  DEFAULT_IMAGE='nginx:latest'
  DEFAULT_PORT=8089

  CONFIG_FILE=${nginx_imageserver_conf}

#work space
  new_workspace

  data_path="${WORD_DIR}/HTML"
  mkdir -p ${data_path}
  conf_path="${WORD_DIR}/conf"
  mkdir -p ${conf_path}
  file_path="${WORD_DIR}/FTP"
  mkdir -p ${file_path}

#param
  setDefaultConfig
#conf
  CONF_FILE="${conf_path}/default.conf"

  #detect file exist
  if [[ -f "${CONF_FILE}" ]]; then
  warn "${CONF_FILE} exist"
  else
  cat >${CONF_FILE} <<EOF
${CONFIG_FILE}
EOF
  fi
#docker command
V1="-v ${data_path}:/usr/share/nginx/html -v ${conf_path}:/etc/nginx/conf.d -v ${file_path}:/mnt/images "
E1=
P1="-p ${PORT}:80"

}



ARGS=`getopt -o "htp:i:s:l:n:d:b:" -l "start:,path:,username:,password:,pwd:,name:,image:,load:,port:,net:,ip:,params:,sql:" -n "getopt.sh" -- "$@"`
eval set -- "${ARGS}"
while true; do
    case "${1}" in
        -s|--start)
        shift;
        if [[ -n "${1}" ]]; then
            CMD=${1}
            echo -e "[**]function name is ${1}"
            shift;
        fi
        ;;
        -d|--path)
        shift;
        if [[ -n "${1}" ]]; then
            BASE_PATH=${1}
            if [[ -f "${BASE_PATH}" ]]; then
            warn "[Menu] Path:"${BASE_PATH}" exist!"
            else
            ok "[Menu] Path:"${BASE_PATH}" Create!"
            mkdir -p ${BASE_PATH}
            fi
            shift;
        fi
        ;;      
        --pwd|--password)
        shift;
        if [[ -n "${1}" ]]; then
            PWD=${1}
            echo -e "[**]password is ${1}"
            shift;
        fi
        ;;   
        --username)
        shift;
        if [[ -n "${1}" ]]; then
            USERNAME=${1}
            echo -e "[**]username is ${1}"
            shift;
        fi
        ;;      
        -n|--name)
        shift;
        if [[ -n "${1}" ]]; then
            NAME_CMD=" --name ${1}"
            NAME=${1}
            echo -e "[**]container name is ${1}"
            shift;
        fi
        ;;    
        -p|--port)
        shift;
        if [[ -n "${1}" ]]; then
            PORT=${1}
            echo -e "[**]port is ${1}"
            shift;
        fi
        ;;
        --net)
        shift;
        if [[ -n "${1}" ]]; then
            SUB_NET=" --net=${1}"
            echo -e "[**]net is ${1}"
            shift;
        fi
        ;;   
        --sql)
        shift;
        if [[ -n "${1}" ]]; then
            SQL=${1}
            echo -e "[**]init_SQL is ${1}"
            shift;
        fi
        ;;      
        --params)
        shift;
        if [[ -n "${1}" ]]; then
            ADDTION_PARAMS=" ${1} "
            echo -e "[**]ADDTION_PARAMS is ${1}"
            shift;
        fi
        ;; 
        --ip)
        shift;
        if [[ -n "${1}" ]]; then
            NET_IP=" --ip=${1}"
            echo -e "[**]ip is ${1}"
            shift;
        fi
        ;;      
        -l|--load)
        shift;
        CMD='load'
        if [[ -n "${1}" ]]; then
            FILE=${1}
            echo -e "[**]target loading file is ${1}"
            shift;
        fi
        ;;  
        -i|--image)
        shift;
        if [[ -n "${1}" ]]; then
            IMAGE=${1}
            echo -e "[**]image:tag is ${1}"
            shift;
        fi
        ;;  
        -h)
        shift;
        help;
        break;
        ;;
        -t)
        OUTPUT='1'
        shift;
        ;;
        --)
        shift;
        break;
        ;;

        *) 
        shift;
        break;
    esac
done
if [[ -n "${CMD}" ]]; then
  eval ${CMD}

  exec_cmd
fi 


function loadImages(){
    if [[ $# -eq 1  &&  -d "$1" ]]; then
      images=`find $1 -name "*.tar"`
      for im in $images;do
        docker load -i $im
    echo "Import $im succeed!"
      done
		elif [[ $# -eq 1  &&  -f "$1" ]]; then
         docker load -i $1
		fi
}
